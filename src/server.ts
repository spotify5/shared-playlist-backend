import { connectDB } from './../config/db';
import express, { Request, Response } from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
var passport = require('passport');
import session from 'express-session';
import mongoose from 'mongoose';

// Load config
dotenv.config({ path: 'config/config.env' });

const server = express();
const PORT = process.env.PORT || 5000;

server.use(cors());

server.use(
  session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
  })
);

server.use(passport.initialize());
server.use(passport.session());

server.use('/user', require('./routes/user'));
server.use('/auth', require('./routes/auth'));
server.use('/', require('./routes/index'));

connectDB().then(() => {
  server.listen(PORT, () => {
    console.log(`Server Started at Port ${PORT}`);
  });
});
