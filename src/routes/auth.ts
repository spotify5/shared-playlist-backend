import express, { Request, Response } from 'express';
const router = express.Router();

var passport = require('passport');
const SpotifyStrategy = require('passport-spotify').Strategy;

import mongoose from 'mongoose';
const User = require('../models/User');

const authCallbackPath = '/spotify/callback';

passport.serializeUser((user: any, done: any) => {
  done(null, user);
});

passport.deserializeUser((id: any, done: any) => {
  User.findById(id, (err: any, user: any) => done(err, user));
});

passport.use(
  new SpotifyStrategy(
    {
      clientID: process.env.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET,
      callbackURL: `http://localhost:8000/auth${authCallbackPath}`
    },
    async (
      accessToken: any,
      refreshToken: any,
      expires_in: any,
      profile: any,
      done: any
    ) => {
      const newUser = {
        spotifyId: profile.id,
        email: profile.emails[0].value,
        displayName: profile.displayName,
        image: profile.photos[0]
      };

      try {
        let user = await User.findOne({ spotifyId: profile.id });
        if (user) {
          done(null, user);
        } else {
          user = await User.create(newUser);
          done(null, user);
        }
      } catch (error) {
        console.error(error);
      }
    }
  )
);

router.get(
  '/spotify',
  passport.authenticate('spotify', {
    scope: ['user-read-email', 'user-read-private'],
    showDialog: true
  })
);

router.get(
  '/spotify/callback',
  passport.authenticate('spotify', { failureRedirect: '/login' }),
  (req: Request, res: Response) => {
    res.redirect('http://localhost:4200/');
  }
);

module.exports = router;
